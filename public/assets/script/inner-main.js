$(function(e) {
  
  var open_modal = true;

  $('.burger-menu a').click(function(e) {
    e.preventDefault();
    if(open_modal) {
      $(this).addClass('burger-menu_active');
      $(".m-body header").addClass('active-m');
      // "slide", { direction: "left" }, 500
    } else {
      $(this).removeClass('burger-menu_active');
      $(".m-body header").removeClass('active-m');
      // "slide", { direction: "left" }, 500
    }
    open_modal = !open_modal;
  });  
  
  function setCookie1(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function setCookie(name, value, options = {}) {

    options = {
      path: '/',
      ...options
    };
  
    if (options.expires instanceof Date) {
      options.expires = options.expires.toUTCString();
    }
  
    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
  
    for (let optionKey in options) {
      updatedCookie += "; " + optionKey;
      let optionValue = options[optionKey];
      if (optionValue !== true) {
        updatedCookie += "=" + optionValue;
      }
    }
  
    document.cookie = updatedCookie;
  }
  
  function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  function deleteCookie(name) {
    setCookie(name, "", {
      'max-age': -1
    })
  }  
  


  function form_active_list(block) {

    var block = block || ".form-slide"; 

    $(block).find('.form_input-list').click(function(e) {
      $(this).find('ul').toggleClass('active')
    }); 

    $(block).find('.form_input-list li').click(function(e) {
      var text = $(this).text().trim();
      if($(this).data('type_d')) {
        $(this).parents('.form_input-sel').find('input').val($(this).data('type_d')); 
      } else {
        $(this).parents('.form_input-sel').find('input').val(text); 
      }
      $(this).parents('.form_input-list').find('span').text(text);
    });

  }

  form_active_list();

  function form_active_file(block) {
    var block = block || '.form-slide';
    $.each($(block).find('.form-f-file'), function(k, v) {

      $(v).click(function(e) {
        $(this).siblings('input[type="file"]').get(0).click();
      });

      $(v).siblings('input[type="file"]').on('change',function(){
        alert($(this).data('a_message'))
      });
   
    });

  }

  form_active_file();


  function remove_block(button, block) {
    $(button).click(function(e) {
      block.remove();
      but_init();
    });
  }  


  function form_show_m(bl, n) {
    var bl = '.form_input_p' || bl;
    var n = '.form_input_n' || n;
    $(bl).change(function(e) {
      $(this).siblings(n).css('display', 'block');  
      but_init();
    });
  }

  form_show_m();

  function form_input_btns(block) {
    var block = block || '.form-slide';
    $(block).find('.form_input-b button').click(function(e) {
      $(this).siblings('button').removeClass('active');
      $(this).addClass('active');
      $(this).siblings('input').val($(this).text().trim());
    });

  }

  function f_data_hid(block) {

    var block = block || '.form-slide'; 

    $(block).find('.f-data-hid').click(function(e) {
      var cls = $(this).data('hcls').split(',');
      var val = $(this).data('hval').split(',');
      
      for (let i = 0; i < cls.length; i++) {
        
        var v = val[i];
        var c = cls[i];

        var cls_n = '.' + c + "-hid";

        $(block).find(cls_n).css('display', v); 
        
        if(v == 'none') {
          $(block).find(cls_n).find('input').addClass('not');
        } else {
          $(block).find(cls_n).find('input').removeClass('not');
        }
      
      }
  
    });

  }

  f_data_hid();

  form_input_btns();

  function multiple_input(plus, minus) {

    var plus = plus || ".form_multiple_input .form_input .plus" ;
    var minus = minus || ".form_multiple_input .form_input .minus" ;

    $(plus).click(function(e) {
      var block  = $(this).parents('.form_input').eq(0).clone();
      block.find('input').val('');
      $(this).parents('.form_multiple_input').eq(0).append(block);      
      

      form_show_m(
        block.find('.form_input_p'),
        block.find('.form_input_n')
      );
      
      remove_block(
        block.find('.minus'),
        block
      );

      but_init();
    });
    
  }

  multiple_input();


  function send_data() {

    var block = block || '#modal-d .content span';
    var modal_d_in = 1;
  
    $('#modal-d').show();
  
    var modal_int = setInterval(() => {
      modal_d_in++;
      if(modal_d_in  == 6)  modal_d_in = 1;
      $('#modal-d .content span').text(".".repeat(modal_d_in));
      
    }, 500);
  
  
    $.each($('.form_input-list'), function(k, v) {
      $(v).siblings('input[type="hidden"]').eq(0).val(
        $(v).find('span').text()
      );
    });
     
    var fd = new FormData;
  
    $.each($('.m-form-a form input').not('.not'), function(k, v) {
  
      var title = $(this).data('title');
      var name = $(this).attr('name');
      var arr_t =  $(this).data('arr');
  
      if(arr_t)
        fd.append(name+'_title[]', title);
      else
        fd.append(name+'_title', title);
      
      
      if($(v).attr('type')!= 'file') {
    
        if(arr_t)
          fd.append(name + '[]', $(this).val());
        else 
          fd.append(name, $(this).val());
      } else {
        if($(v).prop('files').length > 0)
        {
            file = $(v).prop('files')[0];
            if(arr_t)
              fd.append(name + '[]', file);
            else
              fd.append(name, file);
        }
      }
    });
  
    fd.append('more_text', $('textarea[name="more_text"]').val());
    
    $.ajax({
      method: "POST", 
      url: "",
      processData: false,
      contentType: false,
      data : fd
    }).done(function(e) {
      console.log(e);
      clearInterval(modal_int);
      $('#modal-d .content').text('Спасибо, ваша заявка была отправлена');
  
      setTimeout(() => {
        $('#modal-d').hide()
      }, 1500);
  
    }).fail(function(e) {
  
    });
  
  }
  

  var form_l = $('.form-slide-item').length-1;
  var form_i = 0;

  $('.form-slick-n').click(function(e) {
    
    var block = $('.form-slide-item-m');
    var next = $(this).data('next') == "1";
    
    
    if(next) {
      form_i += 1; 
    } else {
      form_i -= 1; 
    }
    
    var end = form_i > form_l-1;
    var end2 = form_i > form_l;

    if(end) {
      if(!end2) {
        $('.form-slick-next').text('Отправить заявку.');     
        block.css('margin-left', (-100 * form_i+1) + '%');
        $('.form-check-d').show();
        $('.form-slick-add').hide();
        $('body, html').animate({scrollTop:0}, 500);        

        var h = $('.form-slide-item').eq(form_i).height();
        $('.form-slide').css('height', h+100); 

        return false;
      } else {
        send_data();
        return false;
      }

    } else {

      block.css('margin-left', (-100 * form_i+1) + '%');
      
      if(form_i > 0) {
        $('.form-slick-prev').show();      
      } else {
        $('.form-slick-prev').hide();      
      }
      
      var h = $('.form-slide-item').eq(form_i).height();
      $('.form-slide').css('height', h+100); 

      $('body, html').animate({scrollTop:0}, 500);
      
    }   

      if(form_i == form_l-1) {
        $('.form-slick-add').show();
        $('.form-slick-next').text('Вперед');     
      } else {
        $('.form-slick-next').text('ВПЕРЕД');     
      }

  });

  function yt_l(block) {
    
    var block = block || ".form-slide";   

    $(block).find('.yt-l-in').on('change', function(e) {
      
      var data = $(this).val();
      
      var matches = data.match(/watch\?v=([a-zA-Z0-9\-_]+)/);
      if(!matches) 
        matches = data.match(/youtu\.be\/([a-zA-Z0-9\-_]+)/);
      
        if(matches) {
        var iframe = $(document.createElement('iframe'));
        iframe.attr('src', 'https://www.youtube.com/embed/'+ matches[1]  +'?autoplay=1&mute=1');
        iframe.attr('allowfullscreen', '1');
        iframe.attr('width', '100%');
        iframe.attr('height', '100%');

        $(block).find('.iframe-yt')
          .html('')
          .css('display', 'block')
          .append(iframe);

        var h = $('.form-slide-item').eq(form_i).height();
        $('.form-slide').css('height', h+100); 
    
      } else {
        $(block).find('.iframe-yt')
          .html('')
          .css('display', 'none');
      }

    });
  }

  yt_l();


  function form_f_v_f(block) {

    var block = block || ".form-slide";   

    $(block).find('.form-f-v-file').click(function(e) {
      $(block).find('.vid input').get(0).click();
    });

    $(block).find('.vid input').on('change',function(){
      alert($(this).data('a_message'));
    });

  }

  form_f_v_f();


  $('.form-slick-add').click(function(e) {

    var block = $('.form-slide-item').eq(1).clone();
    block.html('');

    $.each($('.form_input_copy'), function(k,v) {
      
      var i_block = $(v).clone();
      i_block.removeClass('form_input_copy');
      i_block.addClass('asd');
      block.append(i_block);
      
    });
    
    block.find('.iframe-yt').html('');
    block.find('input[type="text"]').val('');

    yt_l(block);

    multiple_input(
      block.find('.plus'),
      block.find('.minus'),
    );
    
    f_data_hid(block);
    form_f_v_f(block);
    form_active_list(block);
    form_active_file(block);
    form_input_btns(block);

    $('.form-slick-next').text('Этап ' + (form_i+2));
    form_l += 1;
    $('.form-slide .form-slide-item').last().before(block);
    $('.form-slick-next').get(0).click();

  });

  function but_init() {
      var form_d_h = $('.form-slide-item').eq(form_i).height();
      $('.form-slide').css('height', form_d_h + 100); 
  }

  but_init();



  $('.form-check-d').click(function(e) {
    $('.form-slide-item').css('display', 'block');
    
    $('.form-slide').css({
      'white-space' : 'noraml',
      'height' : 'auto'
    });

    $('.form_input_ta').css({
      'margin-left' : '0',
      'margin-right' : '0',
    });

    $('.form-slide').height('');
    $('.form-slide-item-m').css('margin-left', '0');
  }); 


});
  




