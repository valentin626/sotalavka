
var CompData =  [
    {
        'title' :  'Танцевальный клуб Грация', 
        'stage' : 'Лауреат 2 степени', 
        'video' : 'VeG_cGymPIc', 
        'comments' : [
            'Всё отлично, но есть пару моментов. Первый момент, вторая часть танца на втором видео практиче-ски полностью из одного движения. Второй момент, местами не музыкально (возможно это из-за того что на видео так записалось!), но скорее всего видео ни причем. В остальном все хорошо!'
        ], 
        'member' : 'Старкова Мария', 
        'cat_id' : 1,
        'stage_id' : 2, 
        'category_name' : 'Хореография',
        'janr' : 'Бальные танцы', 
    }, 
    {
        'title' :  'Студия танца Аруна',
        'city' : 'Рудный', 
        'stage' : 'Лауреат 2 степени', 
        'stage_id' : 2, 
        'video' : 'VGOYRWV8Y2Y', 
        'comments' : [
            'Девочка умничка, хореография слабовата для неё, нужны более сложные движения, так же местами не музыкально. Чуть-чуть подтянуть и будет отлично! Молодец!'
        ], 
        'teachers' : 'Шерим Аруна Баймахановна',
        'member' : 'Карим Дамиля', 
        'cat_id' : 1,
        'category_name' : 'Хореография',
        'janr' : 'Современный танец', 
    }, 
    {
        'title' :  'Студия танца Аруна',
        'city' : 'Рудный', 
        'stage' : 'Лауреат 2 степени',
        'stage_id' : 2, 
        'video' : 'V7wgmC6zIAc', 
        'comments' : [
            'По-видимому единственный танцевальный номер записанный дома на самоизоляции! Девочка может больше, более сложные элементы будут уместнее, необходимо усложнять! Молодчинка!'
        ], 
        'member' : 'Бледных Елизавета', 
        'age' : '6-7 лет',
        'age_id' : '1',
        'cat_id' : 1,
        'category_name' : 'Хореография',
        'janr' : 'Гимнастика', 
    }, 
    {
        'title' :  'Vikadens school Военный номер хор', 
        'stage' : 'Лауреат 2 степени', 
        'stage_id' : 2, 
        'video' : 'GHjmF6uA450', 
        'comments' : [
            'Нужно отработать танец, не запинаться. Элементы в танце в хорошие. Сюжет замечательный.'
        ], 
        'teachers' : 'Макарщик Виктория Владимировна',
        'member' : 'Макаращак Валерия',  
        'age' : '6-7 лет',
        'age_id' : 1,
        'cat_id' : 1,
        'category_name' : 'Хореография',
        'janr' : 'Народные танцы', 
    }, 
    {
        'stage' : 'Лауреат 1-ой степени!', 
        'stage_id' : 1, 
        'video' : 'Kok5pe-7ny0', 
        'comments' : [
            'Елизавета, талантливая девочка, и поет и танцует!'
        ], 
        'member' : 'Бледных Елизавета',  
        'age' : '6-7 лет',
        'age_id' : 1,
        'cat_id' : 2,
        'category_name' : 'Вокал', 
    }, 
    {
        'stage' : 'Лауреат 1-ой степени! + номинация «ВЫБОР ЗРИТЕЛЯ»', 
        'stage_id' : 1, 
        'video' : 'tTDALj-SWWE', 
        'comments' : [
            'Хорошая песня, хорошее исполнение! Трудно идти к своей цели, но Аружан большая умничка!'
        ], 
        'member' : 'Байборинова Аружан',  
        'age' : '15-18 лет',
        'age_id' : 2,
        'cat_id' : 2,
        'category_name' : 'Вокал', 
    }, 
    {
        'stage' : 'Лауреат 1-ой степени! + номинация «ЛУЧШЕЕ СОЛО ВЕСНЫ 2020 (ВОКАЛ)»', 
        'stage_id' : 1, 
        'video' : 'h0U-rqStKkA', 
        'comments' : [
            'Прекрасное исполнение! Музыкальность. '
        ], 
        'member' : 'Рамазанов Әміре',  
        'age' : '11-14 лет',
        'age_id' : 3,
        'cat_id' : 2,
        'category_name' : 'Вокал', 
    }, 
    {
        'stage' : 'Лауреат 1-ой степени!',
        'stage_id' : 1,
        'video' : 'eZ_Ysz9nkDE', 
        'comments' : [
            'Чистое исполнение! Браво Асылбек!'
        ], 
        'member' : 'Есенгали Асылбек',  
        'age' : '8 лет',
        'age_id' : 4,
        'cat_id' : 3,
        'category_name' : 'Инструментал', 
        'janr' : 'Инструментал', 
    }, 
    {
        'stage' : 'Лауреат 1-ой степени!', 
        'stage_id' : 1, 
        'video' : 'Q8oUcrQFuVA',
        'comments' : [
            'Превосходно! Молодец! Профессиональное исполнение и техника.'
        ], 
        'member' : 'Есенгали Асылбек',  
        'age' : '8 лет',
        'age_id' : 4,
        'cat_id' : 3,
        'category_name' : 'Инструментал', 
        'janr' : 'Инструментал', 
    }, 
    {
        'name' : 'Пони Зефирка',
        'place' : 'детский сад «Mega Kids»',
        'stage' : '1 место', 
        'stage_id' : 3, 
        'image' : '/media/uploads/1.jpg', 
        'comments' : [
            'Отлично чувствует композицию, знает начальные основы компози-ции рисунка.'
        ], 
        'teachers' : 'Дикарева Ирина', 
        'member' : 'Арслан Филиз',  
        'cat_id' : 4,
        'category_name' : 'ИЗО', 
    }, 
    {
        'name' : 'Нету',
        'place' : 'детский сад «Mega Kids»',
        'stage' : '3 место',
        'stage_id' : 5, 
        'image' : '/media/uploads/2.jpg', 
        'comments' : [
            'Нет Эмоций художника. На картинке техника- умения рисовать, построение композиции и передача типичных цветов. Японского аниме.'
        ], 
        'teachers' : 'Дикарева Ирина', 
        'member' : 'Ким Арнур',  
        'cat_id' : 4,
        'category_name' : 'ИЗО', 
    }, 
];


$(function(e) {

    function vi_play(block) {
        
        var block = block || 'body';

        block.find('.video_play').click(function(e) {
            $(this).hide();
            $(this).siblings('.video_iframe').show();
        });

    }


    function show_img(block) {
        $.each(block.find('img'), function(k, v){
            $(v).click(function(e) {
                $('.img-modal-l img').attr('src', $(this).attr('src'));
                $('.img-modal-l').show();
            });
        });
    }




function create_blocks() {

    $('.on-bl-items')
        .find('.on-bl-item')
        .not('.cop').remove();

    var keys = [
        ['title', 'Студия:'], 
        ['member', 'Участник:'],
        ['city', 'Город:'], 
        ['age', 'Возраст:'], 
        ['janr', 'Номинация:'],
        ['stage', 'Призовое место'], 
        ['teachers', 'Хореограф:'],
    ];

    var filter_data = [];
    
    $.each($('.form_input input[type="hidden"]'), function(k,v) {

        if($(v).val() && $(v).val() != 'none') {
            filter_data.push(
                [
                    $(v).attr('name'),
                    $(v).val()
                ]
            );
        }
    });


    var data = CompData.filter( (obj) => {        
        
        var flag = true;
        
        filter_data.forEach( (el_f) => {
            if (obj[el_f[0]] != parseInt(el_f[1])) {
                flag = false;
            }
        });
        return flag; 
    });    


    data.forEach(obj => {

        var m_block = $('.on-bl-item.cop').clone();
        m_block
            .removeClass('cop')
            .removeClass('on-none');

        var list_d = m_block.find('.on-bl-i-desc ul');

        keys.forEach(el_i => {
            
            var li = document.createElement('li');
            if(obj[el_i[0]]) {
                var div = $(document.createElement('div'));
                $(li)
                    .append(
                        div.clone().text(el_i[1])
                    )
                    .append(
                        div.clone().text(obj[el_i[0]])
                    );

                list_d.append($(li));
            }
        });

        obj['comments'].forEach(el => {
            var p = $(document.createElement('p'));
            $(p).text(el);
            m_block.find('.on-bl-i-com').append($(p));
        });


        if(obj['video']) {
            var vi_b = m_block
                .find('.video_iframe iframe');
            vi_b
                .attr('src',  vi_b.attr('src') + obj['video']);       
        } else {

            m_block
                .find('.video_iframe iframe').remove();
            m_block
                .find('.video_play').remove();
          
            var img_d = $(document.createElement('div'));

            img_d.addClass('img_block');
            img_d.append(
                $(document.createElement('img'))
                    .attr('src', obj['image'])
            );

            show_img(img_d);
            
            m_block.find('.on-bl-item-r').append(img_d);

        }

        vi_play(m_block);
        
        $('.on-bl-items').append(m_block);
        
    });

}

    create_blocks();
    
    $('.f-data-category').click(function(e) {
        create_blocks();
    });

    function clear_filter() {
        var blocks = $('.form_input-sel:gt(1)');
        
        $.each(blocks, function(k, v) {
            var block = $(v).find('.form_input-list');
            var li = block.find('li').eq(0);
            block.find('span').text(li.text());
            $(v).find('input[type="hidden"]').val(
                li.data('id')
            );
        });
    }

    $('.f-data-default').click(function(e) {
        clear_filter();
        create_blocks();
    });

    $('.img-modal-l .wrapper').click(function(e) {
        if(e.target == this) 
          $('.img-modal-l').hide();
    });
    
    
    $('.f-cat-img').click(function(e) {
        $('.m-form.fix').css(
            'background-image' , 'url('+ $(this).data('bg') +')'
        );
    });

    if(window.innerWidth < 950) { 
        $('.f-open-list').click(function(e) {
            $("html, body").animate({ scrollTop:  $('.on-bl-title').offset().top }, 1000);
        });
    }

}); 
