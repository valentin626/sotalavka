$(function(e) {

    $(".twentytwenty-container").twentytwenty(
        {default_offset_pct: 0.4}
    );  
    
    var mobile = window.innerWidth < 950;

    var main_sl = $('.reviews-slider').slick({
        accessibility: true,
        adaptiveHeight: mobile,
        arrows: true,
        asNavFor: null,
        prevArrow:  $('.review-btns button').eq(0),  
        nextArrow:  $('.review-btns button').eq(1), 
        autoplay: false,
        autoplaySpeed: 2000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        dots: false,
        dotsClass: 'slick-dots',
        draggable: false,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        focusOnChange: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesToShow: window.innerWidth < 950 ? 1 : 4,
        speed: 1000,
        swipe: false,
        swipeToSlide: false,
        useCSS: true,
        useTransform: true,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      });

    if(mobile)
    var conv_sl = $('.conv-block').slick({
        accessibility: true,
        adaptiveHeight: mobile,
        arrows: true,
        asNavFor: null,
        prevArrow:  $('.conv-buttons button').eq(0),  
        nextArrow:  $('.conv-buttons button').eq(1), 
        autoplay: false,
        autoplaySpeed: 2000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        dots: false,
        dotsClass: 'slick-dots',
        draggable: false,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        focusOnChange: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesToShow: 1,
        speed: 1000,
        swipe: false,
        swipeToSlide: false,
        useCSS: true,
        useTransform: true,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      });


      var quiz_sl = $('.quiz-slider').slick({
        accessibility: true,
        adaptiveHeight: mobile,
        arrows: true,
        asNavFor: null,
        prevArrow:  $('.quiz-b-l button'),  
        nextArrow:  $('.review-btnssdsd button').eq(1), 
        autoplay: false,
        autoplaySpeed: 2000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        dots: false,
        dotsClass: 'slick-dots',
        draggable: false,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        focusOnChange: false,
        infinite: false,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesToShow: 1,
        speed: 1000,
        swipe: false,
        swipeToSlide: false,
        useCSS: true,
        useTransform: true,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      });


    $('.quiz-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log();

        var title = $(slick.$slides.get(nextSlide)).find('.quiz-blocks').eq(0).data('title');
        $('.quiz-title-mod').text(title);
    });


    function quiz_slide(text) { 
        
        var index = parseInt($('.quiz-slider .slick-current').attr("data-slick-index"));             
        $('.quiz-slider').slick('slickGoTo', index + 1);
        $('.quiz-header .nums div').first().text(index + 2);

        if(text)
        $('.quiz-bl-des li')
            .eq(index)
            .find('div').last()
            .text(text);
        
        $('.quiz-b-l').show();

    }

    $('.quiz-b-l button').click(function(e) {
        var index = parseInt($('.quiz-slider .slick-current').attr("data-slick-index"));             
        if(index == 0) {
            $('.quiz-b-l').hide();
        }
        $('.quiz-header .nums div').first().text(index + 1);
    });

    $.each($('.quiz-n'), function(k,v) {
        $(v).find('.quiz-b-n button').click(function(e) {
            
            var text = $(this).data('title');
            quiz_slide(text);

        });
    });

    document.querySelector(".q-cont-edit").addEventListener("input", function(e) {
        $(this)
            .siblings('.quiz-b-button')
            .eq(0)
            .find('button')
            .data('title', $(this).text()); 
    }, false);


    $('.quiz-t').click(function(e) {

    });

    $('.quiz-sel li').click(function(e) {
        
        var text = $(this).text();

        $('.quiz-t span').text();
        $('.quiz-bl-des li')
            .eq(2)
            .find('div').last().
            text(text);
    }); 
        
        
    $('.quiz-input textarea').change(function(e) {
        $('.quiz-bl-des li')
            .eq(3)
            .find('div').last().
            text($(this).val());     
    });

    $('.quiz-in-s input').change(function(e) {
        $('.quiz-bl-des li')
            .eq(parseInt($(this).data('index')))
            .find('div').last().
            text($(this).val());     
    });



    $('form').submit(function(e) {
        e.preventDefault();
		// http://test.localhost/
		$.ajax({ url: "/mail.php", method: "POST", data: $(this).serialize() })
        .done(function (data) {
            alert('Данные успешно отправились');
        })
        .fail(function (err) {
            console.log(err);
            alert('Что-то пошло не так, обратитесь в тех. поддержку');
        });
        return false;
    });    


    $('.show-popup').click(function (e) {
        e.preventDefault();
        $('.popup1').addClass('active');
    });

    $('.popup-head_btn').click(function () {
        $('.popup1').removeClass('active');
    });

    $('.quiz-popup').click(function(e) {
        var ul = $('.quiz-bl-des ul').clone();
        
        ul
            .find('li').last()
            .find('div').last()
            .text(
                $('.quiz-form textarea').val()
            );
        
        var data = [];

        $.each(ul.find('li'), function(k, v) {
            $(v).find('div').last().css('font-weight', 'bold');
            data.push([
                $(v).find('div').first().text(),
                $(v).find('div').last().text(),
            ]); 
        });
            
    
        var obj = {
            'name' : $('.quiz-in-s input[name="name"]').val(),
            'phone' : $('.quiz-in-s input[name="phone"]').val(),
            'message' : JSON.stringify(data),
            'callback_m' : '1',
        };

        $.ajax({ 
            url: "/mail.php", 
            method: "POST", 
            data: obj 
        })
        .done(function (data) {
            alert('Данные успешно отправились');
        })
        .fail(function (err) {
            console.log(err);
            alert('Что-то пошло не так, обратитесь в тех. поддержку');
        });

    
    });

      
    $('#modal-nav .cross').click(function (e) {
        $('#modal-nav').removeClass('active');
    });

    $('.burger').click(function (e) {
        $('#modal-nav').addClass('active');
    });


    $('#modal-nav .modal-list li').click(function(e) {

        var scrol_top = $('.anim-scroll')
            .eq($(this).index())
            .offset().top;

        $('#modal-nav').removeClass('active');

        $('html, body').animate({
            scrollTop: scrol_top
        }, 1500);

    }); 


    var lastScrollTop = 0;
    
    if(mobile)
    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       if (st < lastScrollTop)
           $('.burger').show();
        else 
            $('.burger').hide();
        lastScrollTop = st;
    });

    $(window).scroll(function(event){

        var st = $(this).scrollTop();
        var height = $('.m-header').height(); 

        if (st < height) {
            $('.go-top').hide();
            $('.go-down').show();
        }
        else {
            $('.go-down').hide();
            $('.go-top').show();
        }
        lastScrollTop = st;
    });


    $('.insta-link').click(function(e) {
        window.open($(this).data('link'));
    });

    $('.go-top').click(function(e) {
        $('html, body').animate({
            scrollTop: 0
        }, 1500);          
    });

});