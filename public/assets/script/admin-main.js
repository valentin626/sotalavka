$(function(e) {

  $('.user_list .user_checkbox').click(function(e) {

    var parent = $(this).parents('.user_list').eq(0);
    if(parent.index() == 0) {
      
      if(parent.hasClass('active')) { 
        $('.user_list').removeClass('active');    
      } else {
        $('.user_list').addClass('active');    
      }
      
    } else {
      parent.toggleClass('active');
    }

    $('.user_count span').text($('.user_list.active').not('.not').length);

  });


  $('.show_limit_l_l').click(function(e) {
    $('.show_limit_l_l').toggleClass('active');

    if($(this).hasClass('active'))
      $('.show_limit_l_l').find('ul').show();
    else
      $('.show_limit_l_l').find('ul').hide();

  });


  $('.limit_list_l li').click(function(e) {
    var filter_v = $(this).data('filter');
    $('.filter_limit_l').val(filter_v);
    $('.filter_form').submit();
  });
   
  $('.open_modal').click(function(e) {
    $('#modal').addClass('active');
  });
  
  $('#modal, #modal .cross i').click(function(e) {
    if(e.target == this) $('#modal').removeClass('active');
  }); 

  $('.open_modal_p').click(function(e) {
    $('#modal-p').addClass('active');
  });
  
  $('#modal-p, #modal-p .cross i').click(function(e) {
    if(e.target == this) $('#modal-p').removeClass('active');
  }); 
    

  $('.us-del-user').click(function(e) {
    var self = this;

    $.ajax({
      url: '/del-user',
      method: 'post', 
      data : {
        'action' : 'del',
        'value' :  $(self).data('id')
      }
    }).done(function(e) {
      console.log(e);
      if(e['ok']) {
        alert('Пользователь был упешно создан');
        $(self).parents('ul').eq(0).remove();
      } 
      console.log(e);
    }).fail(function(e) {
      console.log(e);
    });
  });



});